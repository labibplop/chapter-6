const express = require("express");
const bodyParser = require("body-parser");
const { User } = require("./models");

const app = express();
const jsonParser = bodyParser.json();

app.use("/css", express.static(__dirname + "/css"));
app.use("/js", express.static(__dirname + "/js"));
app.set("view engine", "ejs");

// VIEWS
app.get("/dashboard", async (req, res) => {
  const resp = await fetch("http://localhost:3000/user");
  const data = await resp.json();

  res.render("dashboard", { users: data });
});

// CREATE
app.post("/user", jsonParser, async (req, res) => {
  try {
    const dataUser = await User.create({
      nama: req.body.nama,
      email: req.body.email,
      alamat: req.body.alamat,
    });
    res.status(201).send(dataUser);
  } catch (error) {
    res.status(422).send("Data Tidak Berhasil dibuat");
  }
});
// READ
app.get("/user", jsonParser, async (req, res) => {
  const dataUser = await User.findAll();
  res.send(dataUser);
});
// UPDATE
app.put("/user/:id", jsonParser, async (req, res) => {
  try {
    const dataUser = await User.findByPk(req.params.id);
    dataUser.nama = req.body.nama;
    dataUser.email = req.body.email;
    dataUser.alamat = req.body.alamat;
    await dataUser.save();
    res.status(202).send(dataUser);
  } catch (error) {
    res.status(422).send("Data tidak berhasil diUbah");
  }
});
// DELETE
app.delete("/user/:id", async (req, res) => {
  try {
    const dataUser = await User.findByPk(req.params.id);
    dataUser.destroy();
    res.status(202).send("Data Berhasil Dihapus");
  } catch (error) {
    res.status(422).send("Error");
  }
});

app.listen(3000, (req, res) => {
  console.log("APP IS RUNNING");
});
