const handleActionEdit = (elementId) => {
  const el = document.getElementById(elementId);
  const inputNama = document.getElementById("inputNama");
  const inputEmail = document.getElementById("inputEmail");
  const inputAlamat = document.getElementById("inputAlamat");

  inputNama.value = el.children[1].innerText;
  inputEmail.value = el.children[2].innerText;
  inputAlamat.value = el.children[3].innerText;

  document.getElementById("buttonInsert").disabled = true;
  document.getElementById("buttonEdit").disabled = false;

  let userID = elementId.split("-")[2];
  document.getElementById("buttonEdit").onclick = () => {
    handleSubmitEdit(userID);
  };
  //document.getElementById("buttonEdit").setAttribute('onclick',`handleSubmitEdit(${menuId})`)
};

const handleInsertMenu = async () => {
  const inputNama = document.getElementById("inputNama");
  const inputEmail = document.getElementById("inputEmail");
  const inputAlamat = document.getElementById("inputAlamat");

  const resp = await fetch("http://localhost:3000/user", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      nama: inputNama.value,
      email: inputEmail.value,
      alamat: inputAlamat.value,
    }),
  });

  location.reload();
};

const handleSubmitEdit = async (userID) => {
  const inputNama = document.getElementById("inputNama");
  const inputEmail = document.getElementById("inputEmail");
  const inputAlamat = document.getElementById("inputAlamat");

  const resp = await fetch(`http://localhost:3000/user/${userID}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      nama: inputNama.value,
      email: inputEmail.value,
      alamat: inputAlamat.value,
    }),
  });

  location.reload();
};

const handleDelete = async (userID) => {
  alert("Are you sure?");

  const resp = await fetch(`http://localhost:3000/user/${userID}`, {
    method: "DELETE",
  });
  location.reload();
};
